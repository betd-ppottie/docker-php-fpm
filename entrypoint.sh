#!/bin/sh

uid=$(stat -c %u /srv)
gid=$(stat -c %g /srv)

function dotenv () {
  set -a
  [ -f .env ] && ./.env
  set +a
}

dotenv

if [ "enabled" == "$APP_XDEBUG" ]; then
    # Enable xdebug
    ln -sf /xdebug.ini /usr/local/etc/php/conf.d/xdebug.ini
    echo 'Xdebug  is enabled'
else
    # Disable xdebug
    if [ -e /usr/local/etc/php/conf.d/xdebug.ini ]; then
        rm -f /usr/local/etc/php/conf.d/xdebug.ini
    fi
    echo 'Xdebug  is disabled'
fi

if [ $uid == 0 ] && [ $gid == 0 ]; then
    if [ $# -eq 0 ]; then
    	if [ "$APP_ENV" = 'prod' ]; then
	    	bin/console d:m:m --no-interaction --allow-no-migration
	    	bin/console assets:install
    	fi
    	chmod -R 777 /srv/var
        php-fpm --allow-to-run-as-root
    else
        echo "$@"
        exec "$@"
    fi
fi
echo 'Environnement is :'$APP_ENV
sed -i -r "s/foo:x:\d+:\d+:/foo:x:$uid:$gid:/g" /etc/passwd
sed -i -r "s/bar:x:\d+:/bar:x:$gid:/g" /etc/group

sed -i "s/user = www-data/user = foo/g" /usr/local/etc/php-fpm.d/www.conf
sed -i "s/group = www-data/group = bar/g" /usr/local/etc/php-fpm.d/www.conf

user=$(grep ":x:$uid:" /etc/passwd | cut -d: -f1)


if [ $# -eq 0 ]; then
	if [ "$APP_ENV" = 'prod' ]; then
		bin/console doctrine:schema:update --force --no-interaction
		bin/console assets:install
	fi
	chown -R $user /srv/var
    chmod -R 777 /srv/var
    php-fpm
else
    echo gosu $user "$@"
    exec gosu $user "$@"
fi
